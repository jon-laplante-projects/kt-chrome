// Use the chrome.browsingData API to remove browsing data from a user's local profile.
'use strict';

console.log('\'Allo \'Allo! Popup');

$.scoped();

var curr_url;
var url_list;

$(function(){
    function domPercentUpdate(callback){
      var percent = parseInt( $("#analyze_progress_value").text() );
      var iterations = 0;

      $.each(url_list, function(i,e){
        $.get(e.url, function(data){
          $.post("http://localhost:3000/data_consumer", {bestseller_id: e.bestseller_id, ebook_id: e.id, page_html: escape(data)}, function(){
            var t = setTimeout(function(i,e){
              parseInt($("#analyze_progress_value").text(), 10);
              console.log("Percentage: " + percent);
              $("#analyze_progress").css("width", percent + "%");
              $("#analyze_progress_value").text(percent);
              chrome.browserAction.setBadgeText({text: percent + '%'});

              percent = percent + 4;
            }, 100 * i);
          });
        });

          iterations++;
      });

      //if (iterations == 24) {
          callback();
      //}
      /*var percent = parseInt( $("#analyze_progress_value").text(), 10) + 4;
      $("#analyze_progress").css("width", percent + "%");
      $("#analyze_progress_value").text(percent);
      chrome.browserAction.setBadgeText({text: percent + '%'});*/
    }

    chrome.tabs.getSelected(null,function(tab) {
      curr_url = tab.url;
      if ( curr_url.match(/http:\/\/www.amazon.com\/Best-Sellers-Kindle-Store-/g) || curr_url.match(/http:\/\/www.amazon.com\/gp\/bestsellers\/digital-text/g) ) {
        $("#bestsellers_bttn").css("display","none");
        $("#analyze_bttn").css("display","block");
      } else {
        $("#analyze_bttn").css("display","none");
        $("#bestsellers_bttn").css("display","block");
      }
    });

    $("#analyze_bttn").click(function(){
      this.disabled = true;
      //$("#analyze_bttn").prop("disabled","true");
      $("#building_framework").show();
      //$(".progress").show();
      // Add handling for other bestseller URL style.
      $.ajax({
        type: "POST",
        url: "http://localhost:3000/custom_crawl",
        data: { url: curr_url }
      }).success(function(data){
        url_list = data;
        domPercentUpdate(function(){
          // Passing callback function to force sequential execution.
          $("#analyze_bttn").attr("disabled", false);
          $("#building_framework").hide();
          $(".progress").show();
          $("#analyze_progress").css("width","0%");
          $("#analyze_progress_value").text("0");
          //chrome.browserAction.setBadgeText({text: ''});
        });

        //$(".progress").hide();
        //chrome.browserAction.setBadgeText({text: ''});

        /*$.each(data, function(index, resp){
          $.get(resp.url, function(data){
            $.post("http://localhost:3000/data_consumer", {bestseller_id: resp.bestseller_id, ebook_id: resp.id, page_html: escape(data)}, function(){

            });
          });
        });*/
      }).complete(function(){
        /*domPercentUpdate(function(){
          // Passing callback function to force sequential execution.
          $("#analyze_bttn").attr("disabled", false);
          $(".progress").hide();
          $("#analyze_progress").css("width","0%");
          $("#analyze_progress_value").text("0");
          chrome.browserAction.setBadgeText({text: ''});
        });*/
      });
    });
});
